export const RECEIVE_FAMILIES = 'RECEIVE_FAMILIES';
export const TOGGLE_FAMILY = 'TOGGLE_FAMILY';


export function receiveFamilies(families){
  return {
    type: RECEIVE_FAMILIES,
    payload: families
  }
}

export function toggleFamily(familyId){
  return{
    type: TOGGLE_FAMILY,
    id: familyId
  }
}
