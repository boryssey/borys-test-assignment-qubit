import {RECEIVE_FAMILIES, TOGGLE_FAMILY} from '../actions/families'

export default function families(state = {}, action) {
  switch(action.type){
    case RECEIVE_FAMILIES:
      console.log(action)
      const restructuredData = action.payload.map((element, index) => {
        return {
          selected: true,
          id: index,
          family: element
        }

      })
      return {
        ...state,
        data: [...restructuredData]
      }
    case TOGGLE_FAMILY:
      console.log('state', state)
      return {
        ...state,
        data: [...state.data.map((family) => family.id !== action.id ? family :
        Object.assign({}, family, {selected: !family.selected})
      )]}
    default:
      return state
  }

}
