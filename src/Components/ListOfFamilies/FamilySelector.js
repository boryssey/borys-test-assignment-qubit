import React from 'react'
import {MdCheckBox, MdCheckBoxOutlineBlank} from 'react-icons/md'
const FamilySelector = (props) =>{

  return (
    <div onClick = {props.action} className = 'selectorContainer noSelection'>
        {props.selected ? <MdCheckBox className = 'selectedCheckbox'/> : <MdCheckBoxOutlineBlank className = 'notSelectedCheckbox'/>}
    </div>
  )
}


export default FamilySelector;
